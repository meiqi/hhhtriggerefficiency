import os
import sys
import argparse
from math import pi,sqrt,cos
from ctypes import *
import array
import logging
import numpy
import uproot


parser = argparse.ArgumentParser(description='Output trigger efficiency')
parser.add_argument('--inputFiles', dest='inputFiles', action='store',
                    default='', help='comma-separated list of input files (or a single file)')
args = parser.parse_args()

file = uproot.open(args.inputFiles)
trigs = file["trig"]
trigList = [
    # b-jets 2017-2018
    'HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30',
    'HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25',
    'HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21',
    'HLT_j225_gsc300_bmv2c1070_split',
    'HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30',
    'HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25',
    'HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21'
]
eventNumber = {trig: [] for trig in trigList}
for trig in trigList:
    trig_dec = trigs[trig].array(library="np")
    total_num = len(trig_dec)
    bad_jets = sum(1 for i in trig_dec if i == -1)
    pass_jets = sum(1 for i in trig_dec if i == 1)
    print("For", trig, "total eff", pass_jets/total_num,
          "good jets eff", pass_jets/(total_num-bad_jets))
    
    # append event numbers for events passing this trigger to dictionary
    evtNum = trigs[trig + "eventNumber"].array(library="np")
    evtNum = evtNum[evtNum >= 0]
    eventNumber[trig].append(evtNum)

print("Number of bad jets", bad_jets)
trigger_of_interest = [
    'HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25',
    'HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21',
    'HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30',
    'HLT_j225_gsc300_bmv2c1070_split']

common_evtNum = []
unique_evtNum_2b2j = []
unique_evtNum_ht = []
unique_evtNum_2b2j_boosted = []
#unique_evtNum_1b_boosted = []

# unique events passing 2b2j trigger
for num in eventNumber[trigger_of_interest[0]][0]:
    if num not in eventNumber[trigger_of_interest[1]][0]:
        if num not in eventNumber[trigger_of_interest[2]][0]:
            #if num not in eventNumber[trigger_of_interest[3]][0]:
            unique_evtNum_2b2j.append(num)
        else:
            common_evtNum.append(num)

# unique events passing 2b+ht trigger
for num in eventNumber[trigger_of_interest[1]][0]:
    if num not in eventNumber[trigger_of_interest[0]][0]:
        if num not in eventNumber[trigger_of_interest[2]][0]:
            #if num not in eventNumber[trigger_of_interest[3]][0]:
            unique_evtNum_ht.append(num)

# unique events passing 2b2j_boosted trigger
for num in eventNumber[trigger_of_interest[2]][0]:
     if num not in eventNumber[trigger_of_interest[0]][0]:
         if num not in eventNumber[trigger_of_interest[1]][0]:
            #if num not in eventNumber[trigger_of_interest[3]][0]:
            unique_evtNum_2b2j_boosted.append(num)
    
#for num in eventNumber[trigger_of_interest[3]][0]:
#     if num not in eventNumber[trigger_of_interest[0]][0]:
#         if num not in eventNumber[trigger_of_interest[1]][0]:
#            if num not in eventNumber[trigger_of_interest[2]][0]:
#                unique_evtNum_1b_boosted.append(num)

print("2b2j unique eff:", len(unique_evtNum_2b2j)/(total_num-bad_jets))
print('2b+ht unique eff:', len(unique_evtNum_ht)/(total_num-bad_jets))
print("2b2j_boosted unique eff:", len(unique_evtNum_2b2j_boosted)/(total_num-bad_jets))
#print("1b_boosted unique eff:", len(unique_evtNum_1b_boosted)/(total_num-bad_jets))



