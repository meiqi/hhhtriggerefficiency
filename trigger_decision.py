import os
import sys
import argparse
from math import pi,sqrt,cos
from ctypes import *
import array
import ROOT
import logging
import numpy
from glob import glob

# Common pasrameters
GeV = 1000.

parser = argparse.ArgumentParser(description='skim (D)AODs!')

parser.add_argument('-i', '--inputFiles', dest='inputFiles', action='store', nargs="*",
                    default='', help='comma-separated list of input files (or a single file)')
parser.add_argument('-p', '--period', dest='period', type=str,
                    default='', help='period of the data-taking')
parser.add_argument('-t', '--type', dest='type', type=str,
                    default='', help='type of MC events')
parser.add_argument('-o', '--outputFileName', dest='outputFileName',
                    default='tmpTrig.root', help='Output name')


args = parser.parse_args()
inputFiles = args.inputFiles
outputFileName = args.outputFileName

ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )
# Initialize the xAOD infrastructure: 
if(not ROOT.xAOD.Init().isSuccess()): logging.warning("Failed xAOD.Init()")

#inputFiles = inputFiles.split(',')
#print("inputFiles = ", inputFiles)
file_names = list() 
for arg in args.inputFiles:  
    file_names += glob(arg)
fileName=inputFiles[0]
if len(fileName) == 0 :
    print("Please provide input")
    exit()
print("outputFileName = ", outputFileName)

######### Read input tree ##########

treeName = "CollectionTree" # default when making transient tree anyway

ch = ROOT.TChain(treeName)
for infile in file_names :
    ch.Add(infile)
    print (infile)

t = ROOT.xAOD.MakeTransientTree( ch )

######### PRW and LumiCalcFiles for 2016 data ##########
PRWFiles = ["/data/atlas/atlasdata3/maggiechen/HHH6b_signalMC/PRW_files/pileup_mc20e_dsid521162_AF3.root"]
LumiCalcFiles = ["/data/atlas/atlasdata3/maggiechen/HHH6b_signalMC/lumiCalc_files/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                 "/data/atlas/atlasdata3/maggiechen/HHH6b_signalMC/lumiCalc_files/PHYS_StandardGRL_All_Good_25ns_BjetHLT_297730-311481_OflLumi-13TeV-009.root"]

######### Initialize TrigDecision ##########
ROOT.gROOT.ProcessLine("#include \"TrigDecisionTool/TrigDecisionTool.h\"")
ROOT.gROOT.ProcessLine("#include \"TrigConfInterfaces/ITrigConfigTool.h\"")
ROOT.gROOT.ProcessLine("#include \"AsgTools/AnaToolHandle.h\"")
ROOT.gROOT.ProcessLine("#include \"AsgAnalysisInterfaces/IPileupReweightingTool.h\"")
#ROOT.gROOT.ProcessLine("#include \"PileupReweighting/share/DSID521xxx/pileup_mc20e_dsid521162_AF3.root\"")
#ROOT.gROOT.ProcessLine("#include \"GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root\"")
#ROOT.gROOT.ProcessLine("#include \"GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_BjetHLT_297730-311481_OflLumi-13TeV-009.root\"")


ROOT.gROOT.ProcessLine(
    "std::vector<std::string> PRWFiles;\
    std::vector<std::string> lumiCalcFiles;\
    PRWFiles.push_back(\"PileupReweighting/share/DSID521xxx/pileup_mc20e_dsid521162_AF3.root\");\
    lumiCalcFiles.push_back(\"GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root\");\
    lumiCalcFiles.push_back(\"GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_BjetHLT_297730-311481_OflLumi-13TeV-009.root\");\
    asg::AnaToolHandle<TrigConf::ITrigConfigTool> confiTool(\"TrigConf::xAODConfigTool/xAODConfigTool\", nullptr);\
    asg::AnaToolHandle<Trig::TrigDecisionTool> trigDecTool(\"Trig::TrigDecisionTool/TrigDecisionTool\", nullptr);\
    asg::AnaToolHandle<CP::IPileupReweightingTool> prwTool(\"CP::PileupReweightingTool/prw\", nullptr);\
    ASG_MAKE_ANA_TOOL (configTool, TrigConf::xAODConfigTool).ignore();\
    configTool.initialize().ignore();\
    ASG_MAKE_ANA_TOOL (trigDecTool, Trig::TrigDecisionTool).ignore();\
    trigDecTool.setProperty(\"ConfigTool\",configTool).ignore();\
    trigDecTool.setProperty(\"TrigDecisionKey\",\"xTrigDecision\").ignore();\
    trigDecTool.initialize().ignore();\
    ASG_MAKE_ANA_TOOL (prwTool, CP::PileupReweightingTool).ignore();\
    prwTool.setProperty(\"ConfigFiles\",PRWFiles).ignore();\
    prwTool.setProperty(\"LumiCalcFiles\",LumiCalcFiles).ignore();\
    prwTool.initialize().ignore();")

t.GetEntry(0)
print ("Got entry 0")

# 2b+2j triggers in each year
trig_2016 = ["HLT_2j35_bmv2c2070_split_2j35_L14J15",
             "HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25",
             "HLT_2j35_bmv2c2050_split_2j35_L14J15"]
trig_2017 = ["HLT_2j15_gsc35_bmv2c1050_split_2j15_gsc35_boffperf_split_L14J15.0ETA25",
             "HLT_2j25_gsc45_bmv2c1060_split_2j25_gsc45_boffperf_split_L14J15.0ETA25",
             "HLT_2j35_gsc55_bmv2c1070_split_2j35_gsc55_boffperf_split_L14J15.0ETA25"]
trig_2018 = ["HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25",
             "HLT_2j35_gsc45_bmv2c1050_split_2j35_gsc45_boffperf_split_L14J15.0ETA25",
             "HLT_2j45_gsc55_bmv2c1060_split_2j45_gsc55_boffperf_split_L14J15.0ETA25"]

#print ("HLT trigger list: %s" % trigList)

fOut = ROOT.TFile(outputFileName,"RECREATE")
tOut = ROOT.TTree("trig","trig")

# Trigger decisions 
passTrig = {}
eventNumber = {}
if args.period == "2016":
    trig_list = trig_2016
elif args.period == "2017":
    trig_list = trig_2017
elif args.period == "2018":
    trig_list = trig_2018
for trig in trig_list:
    passTrig[trig] = array.array("i",(0 for i in range(0,1)))
    tOut.Branch(trig, passTrig[trig], trig+"/I")
    eventNumber[trig] = array.array("l",(0 for l in range(0,1)))
    tOut.Branch(trig + "_eventNumber", eventNumber[trig],trig+"_eventNumber/L")

######## Start event loop ##########
print( "Number of input events: %s" % t.GetEntries() )
for entry in range( t.GetEntries() ):
    if entry % 5000 == 0:
        print( "On event number: %s" % entry )
    t.GetEntry( entry )
    if args.period == "2016":
        period = int(2016)
        rand_run_num = ROOT.prwTool.GetRandomRunNumber(t.EventInfo.runNumber())
        print(rand_run_num)
    
    # selecting events with at least 4 jets > 40 GeV, and at least 2 more jets > 20 GeV
    # and all jets selected with eta < 2.5
    if hasattr(ch, "AntiKt4EMPFlowJets"):
        jet_pts = []
        jet_etas = []
        for ijet in range(len(ch.AntiKt4EMPFlowJets)) :
            jet = ch.AntiKt4EMPFlowJets[ijet]
            jet_pts.append(jet.pt()/GeV)
            jet_etas.append(jet.eta())
        jet_pts = numpy.array(jet_pts)
        jet_etas = numpy.array(jet_etas)
        boolHard = jet_pts >= 40
        boolSoft = jet_pts <= 40
        boolSoft_req = jet_pts[boolSoft] >= 20
        softish_jets = jet_pts[boolSoft][boolSoft_req]

        for trig in trig_list :
            if len(jet_pts[boolHard]) >= 4 and all(abs(jet_etas[boolHard]) < 2.5):
                if len(softish_jets) >= 2 and all(abs(jet_etas[boolSoft]) < 2.5):
                    # your trigger decisions
                    passTrig[trig][0] = 0
                    if  ROOT.trigDecTool.isPassed( trig ): 
                        passTrig[trig][0] = 1
                        # writing event number 
                        eventNumber[trig][0] = t.EventInfo.eventNumber()
                else:
                    passTrig[trig][0] = -1
                    eventNumber[trig][0] = -1
            else:
                passTrig[trig][0] = -1
                eventNumber[trig][0] = -1
            
    tOut.Fill()

print("Write out trig ntuple ", fOut.GetName() , " with ", tOut.GetEntries(), " events")
fOut.Write()
fOut.Close()

ROOT.xAOD.ClearTransientTrees()


exit(0)