
Currently using `AnalysisBase 21.2.205`. Set this up with:

```
asetup AnalysisBase,21.2.205
```

Trigger are specified in trigList in `trigger_decision.py`, to get trigger decisions run:

```
python trigger_decision.py --inputFiles /path/to/input/file --outputFileName output_file_name
```

Efficiencies are calcuated in `trigger_efficiency.py`, to get them run:

```
python trigger_efficiency.py --inputFiles /output/file/of/trigger_decision
```

